﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamEAPApi.DTO
{
    public class EmployeeDto
    {
        public string EmployeeName { get; set; }
        public long Salary { get; set; }
        public string Department { get; set; }
        public int Gender { get; set; }
    }
}
