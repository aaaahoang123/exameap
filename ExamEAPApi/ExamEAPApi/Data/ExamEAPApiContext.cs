﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ExamEAPApi.Models
{
    public class ExamEAPApiContext : DbContext
    {
        public ExamEAPApiContext (DbContextOptions<ExamEAPApiContext> options)
            : base(options)
        {
        }

        public DbSet<ExamEAPApi.Models.Employee> Employee { get; set; }
    }
}
