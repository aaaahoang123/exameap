﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamEAPApi.DTO;

namespace ExamEAPApi.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long Salary { get; set; }
        public string Department { get; set; }
        public int Gender { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int Status { get; set; }

        public Employee()
        {
        }

        public Employee(string employeeName, long salary, string department, int gender)
        {
            EmployeeName = employeeName;
            Salary = salary;
            Department = department;
            Gender = gender;
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
            Status = 1;
        }

        public Employee(EmployeeDto employee)
        {
            EmployeeName = employee.EmployeeName;
            Salary = employee.Salary;
            Department = employee.Department;
            Gender = employee.Gender;
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
            Status = 1;
        }
    }
}
