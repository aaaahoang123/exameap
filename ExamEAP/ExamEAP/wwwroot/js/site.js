﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var app = angular.module('MyApp', []);
var BASE_URL = 'https://localhost:44367/api/';
app.controller('employeeController', function ($scope, $http) {
    $scope.config = {
        department: ''
    };

    $scope.refreshData = function (e) {
        if (e && e.keyCode !== 13) return;
        $http.get(`${BASE_URL}employees`, { params: $scope.config })
            .then(function ({ data }) {
                $scope.data = data;
                  console.log(data);
            });
    };

    $scope.refreshData();
});

app.controller('createEmployeeController', function($scope, $http) {
    $scope.dto = {
        employeeName: '',
        salary: '',
        department: '',
        gender: '1'
    };

    $scope.doSubmit = function() {
        $http.post(`${BASE_URL}employees`, $scope.dto)
            .then(function ({data}) {
                console.log(data);
                alert("Success");
                location.href = '/employees';
            });
    }
})